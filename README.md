# Epicode books 

Gestione del catalogo di una biblioteca.

## Specifiche

La libreria gestisce nel catalogo **libri** e **periodici**.

Per ogni **libro** � necessario gestire:
- Titolo.
- Autori(possono essere pi� di uno).
- Editore.
- Data di pubblicazione.
- Codice ISBN (se presente).
- Codice interno della biblioteca.
- Numero di copie a disposizione.
- Volumi in cui � strutturato.
- Lingua.
- Categoria di classificazione.
- Numero di pagine totali (di tutti i volumi).

Ogni **copia** ha a disposizione:
- Una posizione nella biblioteca.
- Una indicazione che comunica se disponibile per il prestito.
- Uno stato(in consultazione,in prestito,disponibile).

Ogni **volume** ha:
- Titolo.
- Codice ISBN(se presente).
- Codice interno.
- Numero di pagine.

Ogni **Autore** � caratterizzato da:
- Nome.
- Cognome.
- Nazionalit�.
- Anno di Nascita.
- Anno di morte (se non in vita).

I **periodici** sono caratterizzati da:
- Data di pubblicazione.
- Editore.
- Titolo.
- Categoria di classificazione (anche pi� di una).
- Periodicit�(quotidiano,mensile,annuale,estemporanea,ecc.).

## Permessi
Gli operatori della biblioteca possono gestire il catalogo, mentre gli utenti possono solo effettuare ricerche.


