package it.davide.gestionebiblioteca.types;

public enum Stato {

	IN_CONSULTAZIONE,
	IN_PRESTITO,
	DISPONIBILE
}
