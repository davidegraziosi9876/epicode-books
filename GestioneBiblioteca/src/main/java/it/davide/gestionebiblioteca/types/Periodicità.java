package it.davide.gestionebiblioteca.types;

public enum Periodicità {
	QUOTIDIANO,
	MENSILE,
	ANNUALE,
	ESTEMPORANEA
}
