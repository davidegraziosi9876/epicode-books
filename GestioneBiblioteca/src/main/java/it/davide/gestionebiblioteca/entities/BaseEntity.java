package it.davide.gestionebiblioteca.entities;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class BaseEntity {
	
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO)
	private int id;
	@Column(
            columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", 
            insertable = false, 
            updatable = false)
    private LocalDate createdAt; 
}
