package it.davide.gestionebiblioteca.model;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;

import it.davide.gestionebiblioteca.entities.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Libro extends BaseEntity{
	
	private String titoloLibro;
	@ManyToOne
	private Editore editore;
	private LocalDate dataDiPubblicazione;
	private long codiceISBN;
	private int codiceInternoBiblioteca;
	private int numeroDiCopie;
	@ManyToOne
	private Lingua lingua;
	@ManyToMany
	@JoinTable(
            name="libro_categoria",
            joinColumns= @JoinColumn(name="libro_id", referencedColumnName="id"),
            inverseJoinColumns= @JoinColumn(name="categoria_id", referencedColumnName="id")
        )
	private Set<Categoria> categorie;
	private int numeroPagine;
	
}
