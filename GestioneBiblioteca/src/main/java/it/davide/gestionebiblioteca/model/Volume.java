package it.davide.gestionebiblioteca.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import java.util.Set;

import it.davide.gestionebiblioteca.entities.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Volume extends BaseEntity{

	private String titolo;
    private long codiceISBN;
    private int codiceInterno;
    private int numeroPagine;
    @ManyToOne
    private Libro libri;
}
