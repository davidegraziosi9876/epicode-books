package it.davide.gestionebiblioteca.model;


import javax.persistence.Entity;

import it.davide.gestionebiblioteca.entities.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Lingua extends BaseEntity {
	
	private String lingua;
}
