package it.davide.gestionebiblioteca.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

import it.davide.gestionebiblioteca.entities.BaseEntity;
import it.davide.gestionebiblioteca.types.Stato;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Copia extends BaseEntity{
	
	private String posizione;
	private boolean disponibile;
	@Enumerated(EnumType.STRING)
	private Stato stato;
	@ManyToOne
	private Volume volume;

}
