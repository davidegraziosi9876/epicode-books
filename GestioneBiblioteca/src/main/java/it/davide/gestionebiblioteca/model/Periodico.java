package it.davide.gestionebiblioteca.model;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import it.davide.gestionebiblioteca.entities.BaseEntity;
import it.davide.gestionebiblioteca.types.Periodicità;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Periodico extends BaseEntity {
	
	private String titolo;
	private LocalDate dataDiPubblicazione;
	@ManyToOne
	private Editore editore;
	@ManyToMany
	@JoinTable(
            name="periodico_categoria",
            joinColumns= @JoinColumn(name="periodico_id", referencedColumnName="id"),
            inverseJoinColumns= @JoinColumn(name="categoria_id", referencedColumnName="id")
        )
	private Set<Categoria> categoriaDiClassificazione;
	@Enumerated(EnumType.STRING)
	private Periodicità periodicità;
}
